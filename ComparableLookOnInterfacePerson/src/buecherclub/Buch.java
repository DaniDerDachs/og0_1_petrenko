package buecherclub;

public class Buch implements Comparable<Buch> {

	private String autor;
	private String name;
	private String isbn;

	// Konstruktor

	public Buch(String autor, String name, String isbn) {
		this.setAutor(autor);
		this.setName(name);
		this.setIsbn(isbn);
	}

	// Verwaltungsmethoden

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	// Methoden

	public boolean equals(Buch a) {
		if (a.getIsbn().equals(this.isbn)) {
			a.setAutor(this.autor);
			a.setName(this.name);
			a.setIsbn(this.isbn);
			System.out.println(a + "wurde überschrieben!");
			System.out.println("Das Buch " + a + " heißt nun:" + a.getName());
			return true;

		} else {
			return false;
		}
	}

	// Überschreiben von Systemmethoden(Implementieren von Systemmethoden)

	@Override
	public int compareTo(Buch o) {
		return this.isbn.compareTo(o.isbn);
	}

	@Override
	public String toString() {
		return "Buch [Autor: " + autor + ", Buch: " + name + ", ISBN: " + isbn + "]";
	}

}
