package arraylistandlinkedlist;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArraylistLinkedlist {

	public static void main(String[] args) {
		ArrayList<Integer> zahlenliste = new ArrayList<Integer>();
		//Werte setzen
		for (int i = 0; i < 15; i++) {
			zahlenliste.add(i);
		}
		
		System.out.println(zahlenliste);
		
		//Methoden testen
		System.out.println("add 15");
		zahlenliste.add(15);
		System.out.println("added 15");
		System.out.println("zahlenliste: " + zahlenliste);
		System.out.println("-------------------");
		System.out.println(zahlenliste.size());
		System.out.println("zahlenliste: " + zahlenliste);
		System.out.println("-------------------");
		System.out.println("remove value 15");
		zahlenliste.remove(15);//remove value
		System.out.println("zahlenliste: " + zahlenliste);
		System.out.println("remove value in index 4");
		zahlenliste.remove(4); //remove via index
		System.out.println("zahlenliste: " + zahlenliste);
		System.out.println("-------------------");
		System.out.println("get value of index 3");
		System.out.println(zahlenliste.get(3)); //get via index
		System.out.println("-------------------");
		System.out.println("clear List");
		zahlenliste.clear();
		System.out.println("-------------------");
		System.out.println("zahlenliste: " + zahlenliste);
		
		
	}
	
}
