/**
 * 
 */
package addon;

/**
 * @author user
 *
 */
public class Addon {
	// Attribute
	private int idnummer;
	private String bezeichnung;
	private double preis;
	private String art;
	private int anzahl;
	private int maxAnzahl;

	// Konsturktoren
	public Addon() {

	}

	public Addon(int idnummer, String bezeichnung, double preis, String art, int anzahl, int maxAnzahl) {
		idnummer = this.idnummer;
		bezeichnung = this.bezeichnung;
		preis = this.preis;
		art = this.art;
		anzahl = this.anzahl;
		maxAnzahl = this.maxAnzahl;

	}

	// Methoden
	public void aendereBestand(int bestand) {
		anzahl = bestand;
	}

	public double gesamtwertAddons() {
		return anzahl * preis;
	}
	
	public void setIdnummer(int idnummer) {
		this.idnummer = idnummer;
	}
	
	public int getIdnummer() {
		return idnummer;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	public void setPreis(double preis) {
		this.preis = preis;
	}
	
	public double getPreis() {
		return preis;
	}
	
	public void setArt (String art) {
		this.art = art;
	}
	
	public String getArt() {
		return art;
	}
	
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	public int getAnzahl() {
		return anzahl;
	}
	
	public void setMaxanzahl(int maxAnzahl) {
		this.maxAnzahl = maxAnzahl;
	}
	 public int getMaxanzahl() {
		 return maxAnzahl;
	 }
	
	
}
