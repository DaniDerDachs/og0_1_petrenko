/**
 * 
 */
package addon;

import java.util.Scanner;

/**
 * @author DaniDerDachs
 *
 */
public class AddonTest {
	
	private static int idnummer;
	private static String bezeichnung;
	private static double preis;
	private static String art;
	private static int anzahl;
	private static int maxAnzahl;
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Test Klasse f�r Addon");
		System.out.println("-------------------------------");
		System.out.println("Geben sie eine IDNummer ein: ");
		idnummer = sc.nextInt();
		System.out.println("Geben sie einen Namen ein: ");
		bezeichnung = sc.next();
		System.out.println("Wie teuer ist das Addon ?: ");
		preis = sc.nextDouble();
		System.out.println("Geben sie die Addonart ein: ");
		System.out.println("Hinweis: Tierfutter oder Attraktion");
		art = sc.next();
		System.out.println("Wie viele m�chtest du kaufen ?: ");
		anzahl = sc.nextInt();
		System.out.println("Geben sie eine Maximale Anzahl ein: ");
		maxAnzahl = sc.nextInt();
		
		
		
		Addon addon01 = new Addon(idnummer, bezeichnung, preis, art, anzahl, maxAnzahl);
		
		System.out.println("Ihre ID Nummer lautet: " + addon01.getIdnummer());
		System.out.println("Ihr Name lautet: " + addon01.getBezeichnung());
		System.out.println("Ihr Addon kostet: " + addon01.getPreis());
		System.out.println("Ihre Addonart ist: " + addon01.getArt());
		System.out.println("Sie bestellen " + addon01.getAnzahl() + " Addons");
		System.out.println("Sie k�nnen maximal" + addon01.getMaxanzahl() + "Addons bestellen");
		
		
		
		
		Addon addon02 = new Addon(123, "2 kilo Futter", 2.89, "Tierfutter", 1, 10);
		
		System.out.println("Ihre ID Nummer lautet: " + addon02.getIdnummer());
		System.out.println("Ihr Name lautet: " + addon02.getBezeichnung());
		System.out.println("Ihr Addon kostet: " + addon02.getPreis());
		System.out.println("Ihre Addonart ist: " + addon02.getArt());
		System.out.println("Sie bestellen " + addon02.getAnzahl() + " Addons");
		System.out.println("Sie k�nnen maximal" + addon02.getMaxanzahl() + "Addons bestellen");
		
		sc.close();
		
	}

}
