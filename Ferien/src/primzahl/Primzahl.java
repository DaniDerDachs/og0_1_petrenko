/**  Dieses Programm bestimmt mit einer wie von meinem IT-Lehrer gewŁnschten Methode ob eine Zahl eine Primzahl ist
 * 	 https://de.wikibooks.org/wiki/Primzahlen:_Tabelle_der_Primzahlen_(2_-_100.000) - hier ist eine Liste von Primzahlen mit denen 
 * 	 denen Das Programm getestet werden kann.
 *   Ich habe das Programm mit der Zahl 17 und 4 getestet und es hat geklappt :)
 *   
 */
package primzahl;

/**
 * @author Dachs
 *
 */
public class Primzahl {
	
	public static boolean isPrimzahl(long zahl) {
		boolean ergebnis = false;
		
		for (long i = zahl - 1; i > 1; i--) {
			if (zahl % i != 0) {
				ergebnis = true;
			} // end of if
			
			else if (zahl % i == 0) {
				ergebnis = false;
			}
			
			
		} // end of for
		return ergebnis;
	}

}
