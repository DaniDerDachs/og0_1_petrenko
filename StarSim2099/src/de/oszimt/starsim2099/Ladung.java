package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name) & DaniDerDachs
 * @version (a version number or a date)
 * @version 10.09.2021
 */
public class Ladung {

	// Attribute
	private String typ;
	private int masse;
	private double posX;
	private double posY;
	// Methoden
	
	public Ladung() {
		this.typ = "";
		this.masse = 0;
		this.posX = 0;
		this.posY = 0;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}
	
	public String getTyp() {
		return this.typ;
	}
	
	public int getMasse() {
		return this.masse;
	}
	
	public double getPosX() {
		return this.posX;
	}
	
	public double getPosY() {
		return this.posY;
	}
	

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}