package rechner;

import java.util.Scanner;

public class Rechner {
	public static void main(String[] args) {
		while (true) {
			String eingabe1;
			int zahl1, zahl2;

			System.out.println("Geben Sie eine ganze Zahl ein (x zum beenden): ");
			eingabe1 = new Scanner(System.in).nextLine();

			if (eingabe1.equalsIgnoreCase("x"))
				break;

			try {
				zahl1 = Integer.parseInt(eingabe1);
				System.out.println("Geben Sie eine ganze Zahl ein: ");
				zahl2 = Integer.parseInt(new Scanner(System.in).nextLine());

				int zahl3 = zahl1 + zahl2;
				
				//Doppelt Integer Overflow weil das manhcmal nicht exceptioniert
				if (zahl1 > Integer.MAX_VALUE - zahl2) {
					throw new ArithmeticException("Integer overflow");
				} else if (zahl1 < Integer.MIN_VALUE + zahl2) {
					throw new ArithmeticException("Integer Underflow");
				}
				System.out.printf("%d + %d = %d%n", zahl1, zahl2, Math.addExact(zahl1, zahl2));
				System.out.printf("%d - %d = %d%n", zahl1, zahl2, Math.subtractExact(zahl1, zahl2));
				System.out.printf("%d * %d = %d%n", zahl1, zahl2, Math.multiplyExact(zahl1, zahl2));
				System.out.printf("%d / %d = %d%n", zahl1, zahl2, zahl1 / zahl2); //Kein Overflow oder Underflow möglich

			} catch (ArithmeticException ae) {
				System.err.println("\n Arithmetische Exception \n");
				ae.printStackTrace();
			} catch (NumberFormatException nfe) {
				System.err.print("\n Ungültige Eingabe \n");
				nfe.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
}
