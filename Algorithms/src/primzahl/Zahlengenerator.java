package primzahl;

import java.util.Scanner;
import java.util.Random;



public class Zahlengenerator  {
	
	public static void main(String[] args) {
		char c = 'n';
		System.out.println(Long.MAX_VALUE);
		Scanner sc = new Scanner(System.in);
		System.out.println("Möchten sie eigene Zahlen eingeben ?");
		c = sc.next().charAt(0);
		long time = System.currentTimeMillis();
		switch (c) {
		
		case 'n':
			long[] zahlen = zufallszahlengenerator();
			for (int i = 1; i < zahlen.length; i++) {
				System.out.print(zahlen[i] + "\t");
				System.out.print(primzahlchecker(zahlen[i]) + "\t");
				System.out.println(System.currentTimeMillis() - time + "ms");
			}
			
			
			break;

		case 'j':
			System.out.print("Wie viele ?");
			System.out.println("");
			int x = sc.nextInt();
			long[] eingaben = new long[x];
			for (int i = 0; i < eingaben.length; i++) {
				System.out.print("Eingabe: ");
				eingaben[i] = sc.nextLong();
				time = System.currentTimeMillis();
				System.out.println("");
			System.out.println(primzahlchecker(eingaben[i]));
			System.out.println(System.currentTimeMillis() - time + "ms");
			}
			System.out.println();
			break;
			
			
		default:
			System.out.println("Nur j und n als Antwort m�glich");
			
			break;
		}
		sc.close();
	} // ende der Main
	

	public static long[] zufallszahlengenerator() {
		//BufferedWriter f = null;
		Random rand = new Random();
		long randomnumber = 0;
		
		long[] zahlen = new long[11];
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = randomnumber;
			randomnumber = rand.nextLong();
			if (randomnumber <= 0) {
				randomnumber = randomnumber *(-1);
			}
		}

		
		
		/*for (int i = 1; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
			//long zahl = zahlen[i];
			String zahlS = Long.toString(zahlen[i]);
			try {
				f = new BufferedWriter(new FileWriter("Zahlen" + i + ".txt"));
				f.write(zahlS);
				f.newLine();
			} finally {
				f.close();
			}*/ //Ursprünglich sollte ein textdokument erstellt werden welcher dann von der Methode 
				//primzahlchecker gecheckt werden sollte, da ich den Code anders mache ist das aber nicht mehr Notwendig
				return zahlen;
			} 
			
			
	
	public static boolean primzahlchecker(long zahl) { // Methode aus dem letzten Schuljahr(zumindestens auf dem Stick gefunden, könnte also auch aus dem Internetz sein(Bitte keine Minus Punkte :D))
		
		
		
		if (zahl <= 16) {
            return (zahl == 2 || zahl == 3 || zahl == 5 || zahl == 7 || zahl == 11 || zahl == 13);
        }
        if (zahl % 2 == 0 || zahl % 3 == 0 || zahl % 5 == 0 || zahl % 7 == 0) {
            return false;
        }
        for (long i = 10; i * i <= zahl; i += 10) {
            if (zahl % (i+1) == 0) {  
                return false;
            }
            if (zahl % (i+3) == 0) { 
                return false;
            }
            if (zahl % (i+7) == 0) {  
                return false;
            }
            if (zahl % (i+9) == 0) {  
                return false;
            }
        }
        return true;
		
	}
	
	/*public static void patek() {
		Timer timer = new Timer();
		TimerTask task = new TimerTask() {
			public int i = 0;
			@Override
			public void run() {
				i++;	
			}
			
		};
	timer.schedule(task, 0);	
	}
	*/
	
}

	


