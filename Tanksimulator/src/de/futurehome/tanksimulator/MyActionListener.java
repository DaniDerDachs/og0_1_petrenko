package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			int eingabe = f.slider.getValue();
			double fuellstand = f.myTank.getFuellstand();
			if (fuellstand >= 100) {
				fuellstand = 100;
			}

			else {
				fuellstand = fuellstand + eingabe;
			}

			if (fuellstand <= 0) {
				fuellstand = 0;
			}
			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText("" + fuellstand);
			f.lblProzent.setText(fuellstand + "%");
		}
		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = fuellstand - 2;
			if (fuellstand <= 0) {
				fuellstand = 0;
			}
			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText("" + fuellstand);
			f.lblProzent.setText(fuellstand + "%");
		}
		if (obj == f.btnZuruecksetzen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText("" + fuellstand);
			f.lblProzent.setText(fuellstand + "%");
		}

	}
}