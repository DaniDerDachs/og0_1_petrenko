package de.futurehome.tanksimulator;
public class Tank {
	
	private double fuellstand;

	public Tank(double fuellstand) {
		this.fuellstand = fuellstand;
	}

	public double getFuellstand() {
		
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		if (fuellstand >= 100) {
			fuellstand = 95;
		}
		this.fuellstand = fuellstand;
	}

}
