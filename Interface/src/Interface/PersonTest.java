package Interface;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dachs
 *
 */
public class PersonTest {

	public static void main(String[] args) {

		Person p0 = new Person("Daniel", LocalDate.of(2004, Month.SEPTEMBER, 16));
		Person p1 = new Person("Paul", LocalDate.of(2004, Month.JUNE, 1));
		Person p2 = new Person("Robin", LocalDate.of(2003, Month.MARCH, 17));
		Person p3 = new Person("Tim", LocalDate.of(1920, Month.SEPTEMBER, 16));
		Person p4 = new Person("Maria", LocalDate.of(2020, Month.SEPTEMBER, 16));
		Person p5 = new Person("Max", LocalDate.of(2004, Month.JULY, 22));
		Person p6 = new Person("Erva", LocalDate.of(2002, Month.JANUARY, 2));
		Person p7 = new Person("Siad", LocalDate.of(2002, Month.JANUARY, 7));
		Person p8 = new Person("Daniel2", LocalDate.of(2004, Month.SEPTEMBER, 16));
		Person p9 = new Person("Majd", LocalDate.of(2004, Month.FEBRUARY, 29));

		// System.out.println(p0.toString());

		List<Person> personen = new ArrayList<Person>(List.of(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9));

		System.out.println(personen);

		personen.sort(null);
		

		System.out.println(personen);

	}

}
