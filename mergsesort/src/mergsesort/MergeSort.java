/*
 * MergeSort by Daniel und Karim
 * 19.11.2021
 * 
 * 
 */

package mergsesort;

import java.util.Random;

public class MergeSort {

	private static int zahler = 0;

	public static void main(String[] args) {
		int[] liste = zufallszahlengenerator();
		//int[] liste = {9,2,4,5,6,1,2,3,7,8};
		int[] nArr = mergesort(liste);
		System.out.println("mergesort: ");
		System.out.println("------------------------------");
		for (int i = 0; i < nArr.length; i++) {
			if (i < nArr.length - 1) {
				System.out.print(nArr[i] + "->");
			}
			else {
				System.out.println(nArr[i]);
			}
		}
		System.out.println("Vergleiche: " + getZahler());

	}

	public static int[] mergesort(int[] liste) {
		zaehlen();
		if (liste.length > 1) {
			int mid = (int) (liste.length / 2);
			int[] links = new int[mid];
			for (int i = 0; i < mid; i++) {
				links[i] = liste[i];
			}
			int[] rechts = new int[liste.length - mid];
			for (int i = mid; i < liste.length; i++) {
				rechts[i - mid] = liste[i];
			}

			links = mergesort(links);
			rechts = mergesort(rechts);
			return merge(links, rechts);
		} else {
			return liste;
		}

	}

	protected static int[] merge(int[] links, int[] rechts) {

		int[] nListe = new int[links.length + rechts.length];
		int idxLinks = 0;
		int idxRechts = 0;
		int idxMid = 0;
		while (idxLinks < links.length && idxRechts < rechts.length) {
			zaehlen();
			if (links[idxLinks] < rechts[idxRechts]) {
				nListe[idxMid] = links[idxLinks];
				idxLinks++;
			} else {
				nListe[idxMid] = rechts[idxRechts];
				idxRechts++;
			}
			idxMid++;
		}
			while (idxLinks < links.length) {

				nListe[idxMid] = links[idxLinks];
				idxLinks++;
				idxMid++;
			}

			while (idxRechts < rechts.length) {
				nListe[idxMid] = rechts[idxRechts];
				idxRechts++;
				idxMid++;
			}

		

		return nListe;
	}

	public static int[] zufallszahlengenerator() {

		Random rand = new Random();
		int randomnumber = 0;

		int[] zahlen = new int[21];
		for (int i = 1; i < zahlen.length; i++) {
			zahlen[i] = randomnumber;
			randomnumber = rand.nextInt();
			if (randomnumber < 0) {
				randomnumber = randomnumber * (-1);
			}
		}
		return zahlen;
	}

	public static void zaehlen() {
		zahler++;
	}

	public static int getZahler() {
		return zahler;
	}

}
