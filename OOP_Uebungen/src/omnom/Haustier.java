package omnom;

public class Haustier {
	
	//Attribute
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;
	
	
	//Konstruktor
	public Haustier() {
		
	}
	public Haustier(String name) {
		this.name = name;
	}
	
	public Haustier(int hunger, int muede, int zufrieden, int gesund, String name) {
		this.hunger = 100;
		this.muede = 100;
		this.zufrieden = 100;
		this.gesund = 100;
		this.name = "omNom";
		
	}
	
	//Verwaltungsmethoden(setter und getter)
	public int getHunger() {
		return this.hunger;
	}
	
	public void setHunger(int hunger) {
		this.hunger = hunger;
		if (hunger == 0 && hunger <= 100) {
			this.hunger = hunger;
		}
		else {
			this.hunger = hunger;
		}
			
	}
	
	public int getMuede() {
		return this.muede;
	}
	
	public void setMuede(int muede) {
		this.muede = muede;
		if (muede == 0 && muede <= 100) {
			this.muede = muede;
		}
		else {
			this.muede = muede;
		}
	}
	
	public int getZufrieden() {
		return this.zufrieden;
	}
	
	public void setZufrieden(int zufrieden) {
		this.zufrieden = zufrieden;
		if (zufrieden == 0 && zufrieden <= 100) {
			this.zufrieden = zufrieden;
		}
		else {
			this.zufrieden = zufrieden;
		}
	}
	
	public int getGesund() {
		return this.gesund;
	}
	
	public void setGesund(int gesund) {
		this.gesund = gesund;
		if (gesund == 0 && gesund <= 100) {
			this.gesund = gesund;
		}
		else {
			this.gesund = gesund;
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	//Tuen
	public void fuettern(int anzahl) {
		this.hunger -= anzahl;
		if (this.hunger <= 0) {
			this.hunger = 0;
		}
		
	}
	
	public void schlafen(int dauer) {
		this.muede += dauer;
		if (muede == 0 && muede <= 100) {
			this.muede = 100;
		}
		
		
	}
	
	public void spielen(int dauer) {
		this.zufrieden += dauer;
		if (zufrieden == 0 && zufrieden <= 100) {
			this.zufrieden = 100;
		}
		
	}
	
	public void heilen() {
		this.gesund = 100;
	}
	
}
