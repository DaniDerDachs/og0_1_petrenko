package arrays;

public class ArrayA {

	public static void main(String[] args) {
		int[] zahlen = new int[100]; // Array der gr��e 100 wird generiert
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.println(i);
		}
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i + 1;
		}
		
		System.out.println(zahlen[89]); // Ausgabe des Wertes der Stelle 89 im Array

		zahlen[49] = 1060;
		zahlen[0] = 2020;
		zahlen[99] = 2020;
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
		
		double summe = 0;
		double durchschnitt = summe;
		for (int i = 0; i < zahlen.length; i++) {
			summe += i;
		}
		durchschnitt = summe / 100;
		System.out.println(durchschnitt);
		
	}

}
