/**
 * 
 */
package oszimt;

import java.util.Scanner;

/**
 * @author Daniel
 *
 */
public class Sum {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben sie einen Grenzwert ein.");
		int gw = sc.nextInt();
		gw *= 2;
		int summe = 0;
		
		for (int i = 0; i <= gw; i = i + 2) {
			summe += i;
		}
		System.out.println("Die Summe lautet: " + summe);
		sc.close();

	}

}
