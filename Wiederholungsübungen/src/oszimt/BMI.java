/**
 * 
 */
package oszimt;

import java.util.Scanner;

/**
 * @author Daniel
 *
 */
public class BMI {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Geschlecht eingeben (m/w)");
		char geschlecht = sc.next().charAt(0);
		System.out.println("Gewicht eingeben");
		double gewicht = sc.nextDouble();
		System.out.println("Gib deine Körpergröße");
		System.out.println("In Centimeter (z.b. 190)");
		double koerpergroesse = (sc.nextDouble() / 100);
		double bmi = gewicht / (koerpergroesse * koerpergroesse);
		System.out.println("Dein BMI ist:" + bmi);
		if (bmi < 20 && geschlecht == 'm' || bmi < 19 && geschlecht == 'w') {
			System.out.println("Untergewichtig");
		}

		if (bmi > 25 && geschlecht == 'm' || bmi > 24 && geschlecht == 'w') {
			System.out.println("Übergewichtig");
		} // end of if

		if ((bmi >= 20 && bmi < 25) && geschlecht == 'm' || (bmi >= 19 && bmi <= 24) && geschlecht == 'w'  ) {
			System.out.println("Normalgewicht");
		} // end of if
		sc.close();
	} // end of main

} //end of class
