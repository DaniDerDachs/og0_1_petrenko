package binarySearch;

import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class BinarySearch {
	static long time;

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Wie groß soll das Array sein ?");
		System.out.println("Hinweis: Das Array muss größer sein als 15 Million und kleiner als 20 Million.");
		int size = sc.nextInt();
		if (size < 15000000 || size > 20000000) {
			if (size < 15000000) {
				size = 15000000;
			} else {
				size = 20000000;
			}
		}
		System.out.print("Welche Zahl soll gesucht werden ?: ");
		long zuSuchen = sc.nextLong();
		long[] arr = lineArray(size);
		System.out.println("Bitte wählen sie die Binäre Suche(s1), Interpolare Suche(s2) oder die Binäre Suchein rekusriv(s3) ");
		String wahl = sc.next();
	
		switch (wahl) {
		case "s1":
			//time = System.currentTimeMillis();
			System.out.println("Gefunden bei: " + searcher(arr, zuSuchen));
		//	System.out.println((System.currentTimeMillis() - time) + "ms");
			break;
		case "s2":
			//time = System.currentTimeMillis();
			searcher2(arr, zuSuchen);
			//System.out.println((System.currentTimeMillis() - time) + "ms");
			break;
		case "s3":
			int[] intarr = intlineArray(size);
			System.out.println("Gefunden bei: " + searcher3(intarr, 0, arr.length, zuSuchen));
			
			break;

		default:
			break;
		}

		sc.close();

	}

	public static long[] randomArray(int size) { // int max größe: 2 147 483 647

		Random rand = new Random();
		long[] zahlen = new long[size];
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = rand.nextLong();
		}

		for (int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}

		return zahlen;
	}

	/*
	 * public static long[] sortArray(long[] zahlen) {
	 * 
	 * Arrays.sort(zahlen);
	 * 
	 * Test Schleife for (int i = 0; i < zahlen.length; i++) {
	 * System.out.println(zahlen[i]); }
	 * 
	 * return zahlen; }
	 */

	public static long[] lineArray(int size) {
		long[] arr = new long[size];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i + 1;
			// System.out.println(arr[i]);
		}

		return arr;
	}
	
	public static int[] intlineArray(int size) {
		int[] arr = new int[size];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i + 1;
			// System.out.println(arr[i]);
		}

		return arr;
	}

	public static int searcher(long[] zahlen, long zuSuchen) {

		int min = 0;
		int max = zahlen.length - 1;
		int anlaeufe = 0;

		while (min <= max) {

			int mid = min + ((max - min) / 2);
			anlaeufe++;
			if (zuSuchen == zahlen[mid]) {
				// System.out.println("Die gesucht Zahl befindet sich im Index: " + mid);
				System.out.println("Anl�ufe: " + anlaeufe);
				return mid;
			} else {
				if (zahlen[mid] > zuSuchen) {

					max = mid - 1;
				} else {
					min = mid + 1;
				}
			}

		}
		System.out.println("Anl�ufe: " + anlaeufe);
		return -1;
	}

	public static void searcher2(long[] zahlen, long zuSuchen) {

		// cast auf int da das Array maximal 20 Million sein darf und in unser Integer
		// Zahlenraum fällt.
		long max = zahlen.length - 1;
		long min = 0;
		int anlauefe = 0;
		while (zuSuchen >= zahlen[(int) min] && zuSuchen <= zahlen[(int) max]) {

			long pos = min + (max - min) * (zuSuchen - zahlen[(int) min]) / (zahlen[(int) max] - zahlen[(int) min]);
			anlauefe++;
			if (zahlen[(int) pos] == zuSuchen) {
				System.out.println("Gefunden bei: " + pos);
				System.out.println("Versuche: " + anlauefe);
				break;
			} else if (zahlen[(int) pos] < zuSuchen) {
				min = pos + 1;
			} else {
				max = pos - 1;
			}

		}

	}
	
	public static int searcher3(int[] zahlen, int min, int max, long zuSuchen) {
		if (max >= min && min <= zahlen.length - 1) {
			int mid = min + ((max - min) / 2);
			if (zahlen[mid] == zuSuchen) return mid;
			
			if (zahlen[mid] > zuSuchen) return searcher3(zahlen, min, mid - 1, zuSuchen);
			
			return searcher3(zahlen, mid + 1, max, zuSuchen);
			
			
		}
		return -1;
		
	}
	
}
