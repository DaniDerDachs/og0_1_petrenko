package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;

public class TauglichkeitstesterGUI extends JFrame {

	private JPanel MainBorder;
	private JTextField tfdText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TauglichkeitstesterGUI frame = new TauglichkeitstesterGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TauglichkeitstesterGUI() {
		setTitle("Tauglichkeitstester");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 583, 458);
		MainBorder = new JPanel();
		MainBorder.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(MainBorder);
		MainBorder.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlCenterMain = new JPanel();
		MainBorder.add(pnlCenterMain, BorderLayout.CENTER);
		pnlCenterMain.setLayout(new BorderLayout(0, 0));
		
		JPanel pnl = new JPanel();
		pnlCenterMain.add(pnl, BorderLayout.CENTER);
		pnl.setLayout(new BorderLayout(0, 0));
		
		JEditorPane editorPane = new JEditorPane();
		pnl.add(editorPane);
		
		JPanel pnlUseMethod = new JPanel();
		pnl.add(pnlUseMethod, BorderLayout.SOUTH);
		pnlUseMethod.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JRadioButton rdbtnReaktionstest = new JRadioButton("Reaktionstest");
		pnlUseMethod.add(rdbtnReaktionstest);
		
		JRadioButton rdbtnKonzentrationstest = new JRadioButton("Konzentrationstest");
		pnlUseMethod.add(rdbtnKonzentrationstest);
		
		JRadioButton rdbtnEinschaetzungstest = new JRadioButton("Einschaetzungstest");
		pnlUseMethod.add(rdbtnEinschaetzungstest);
		
		JPanel pnlTextandButtons = new JPanel();
		pnlCenterMain.add(pnlTextandButtons, BorderLayout.EAST);
		pnlTextandButtons.setLayout(new BorderLayout(0, 0));
		
		tfdText = new JTextField();
		pnlTextandButtons.add(tfdText, BorderLayout.NORTH);
		tfdText.setColumns(10);
		
		JPanel pnlButtons = new JPanel();
		pnlTextandButtons.add(pnlButtons, BorderLayout.SOUTH);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Stopuhr und ausgewählte Klasse starten
			}
		});
		pnlButtons.add(btnStart);
		
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Stopuhr stoppen und Prozess stoppen(nicht das GUI schließen!)
			}
		});
		pnlButtons.add(btnStop);
	}

}
