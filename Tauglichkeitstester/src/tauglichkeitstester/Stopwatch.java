package tauglichkeitstester;

import java.util.Timer;
import java.util.TimerTask;
 
class Helper extends TimerTask {
    public static int i = 1;
     
     
    public void run() {
        System.out.println("This is called " + i++ + " time");
    }
}
 
public class Stopwatch {
    public static void main(String[] args) {
 
        Timer timer = new Timer();
         
        
        TimerTask task = new Helper();
         
        
         
        timer.schedule(task, 200, 1); //jede millisekunde kommt eine Uhrzeit
 
    }
}