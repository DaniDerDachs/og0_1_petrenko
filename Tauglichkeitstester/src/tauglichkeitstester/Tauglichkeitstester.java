package tauglichkeitstester;

import java.awt.*;
import java.awt.Canvas;

public abstract class Tauglichkeitstester {
	
	//Attribute
	protected boolean aktiv;
	protected double ergebnis;
	protected Canvas canvas;
	protected Rechteck rec;
	protected int xMid;
	protected int yMid;
	
	protected abstract void warten();
	
	public abstract String getErgebnis();
	
	public boolean isAktiv() {
		return this.aktiv;
	}
	
	public abstract void starten();
	
	public abstract void stoppen();
	
	public abstract void zeigeHilfe();
	
	
	public abstract void create(Canvas canvas, Rechteck rec);
	
	
}
