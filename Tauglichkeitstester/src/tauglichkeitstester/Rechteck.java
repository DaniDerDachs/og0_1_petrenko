package tauglichkeitstester;

public class Rechteck {
	private double a;
	private double b;
	
	public Rechteck() {
		this.a = 0;
		this.b = 0;
	}
	
	public Rechteck(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	public double getB() {
		return b;
	}
	public void setB(double b) {
		this.b = b;
	}
	
	
}
