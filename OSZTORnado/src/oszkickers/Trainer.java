package oszkickers;

public class Trainer extends Mitglied {
	// Variables
	private char lizensklasse;
	private double aufwandentschaedigung;

	// Constructor
	public Trainer(String name_new, String tel_new, boolean jahr_new, char lizens_new, double entschaedigung_new) {
		super(name_new, tel_new, jahr_new);

		lizensklasse = lizens_new;
		aufwandentschaedigung = entschaedigung_new;
	}

	// Methods
	public char getLizensklasse() {
		return lizensklasse;
	}

	public void setLizensklasse(char lizens_new) {
		this.lizensklasse = lizens_new;
	}

	public double getAufwandentschaedigung() {
		return aufwandentschaedigung;
	}

	public void setAufwandentschaedigung(double entschaedigung_new) {
		this.aufwandentschaedigung = entschaedigung_new;
	}
}
