import java.util.ArrayList;

public class KeyStore02 {

	private ArrayList<String> aal = new ArrayList<String>();

	public KeyStore02() {
		this.aal = new ArrayList<String>();
	}

	public KeyStore02(int len) {
		aal = new ArrayList<String>(len);
	}
	
	public boolean add(String eintrag) {
		aal.add(eintrag);
		return true;
	}
	
	public int indexOf(String eintrag) {
		return aal.indexOf(eintrag);
	}
	
	public void remove(int index) {
		aal.remove(index);
	}

}
