import java.util.Arrays;

public class KeyStore01 {
	private String[] keyList01;
	private int pos;
	private final int MAXPOS = 100;

	public KeyStore01() {
		this.keyList01 = new String[MAXPOS];
		this.pos = 0;
	}

	public KeyStore01(int len) {
		this.keyList01 = new String[len];
		this.pos = 0;
	}

	public boolean add(String eintrag) {
		if (pos < MAXPOS) {
			keyList01[pos] = eintrag;

			this.pos += 1;

			return true;
		}

		return false;
	}

	public int indexOf(String eintrag) {

		for (int i = 0; i < pos; i++) {
			if (this.keyList01[i].equals(eintrag)) {
				return i;
			}
		}

		return -1;
	}

	public void remove(int index) {
		if (index >= 0 && index < pos) {
			for (int i = index; i < pos; i++) {
				this.keyList01[i] = this.keyList01[i + 1];
				
			}
			pos -= 1;
		}

	}

	@Override
	public String toString() {
		return "KeyStore01 [keyList01=" + Arrays.toString(keyList01) + ", pos=" + pos + ", MAXPOS=" + MAXPOS + "]";
	}

}