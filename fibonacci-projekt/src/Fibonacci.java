/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci{
   // Konstruktor
   Fibonacci(){
   }
   
  /**
    * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
    * @param n 
    * @return die n-te Fibonacci-Zahl
   */
  long fiboRekursiv(int n){
      if(n == 0) {
    	  return 0;
      }
      else if (n == 1) {
		return 1;
	}
      else  {
		return fiboRekursiv(n-1) + fiboRekursiv(n-2);
	}
  }//fiboRekursiv

  /**
   * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
   * @param n 
   * @return die n-te Fibonacci-Zahl
  */
  long fiboIterativ(int n){
	  int[] fibo = new int[n + 2];
	  fibo[0] = 0;
	  fibo[1] = 1;
	  
	for (int i = 2; i <= n; i++) {
		
		fibo[i] = fibo[i-1]+fibo[i-2];
		
		
	}  
	
	return fibo[n];
  }//fiboIterativ

}// Fibonnaci


